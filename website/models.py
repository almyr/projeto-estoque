from django.db import models

class Product(models.Model):
    name = models.CharField(max_length=45)
    description = models.TextField()
    code = models.CharField(max_length=20)
    image = models.ImageField(null=True, upload_to='products')
    quantity = models.PositiveIntegerField()

